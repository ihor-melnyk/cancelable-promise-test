class CancelablePromise extends Promise {
    static #instances = [];
    #reject;
    constructor(executor) {
        if(typeof executor !== 'function') {
            throw new Error('Wrong constructor argument');
        }
        let rejectHandler;
        super((resolve,reject) => {
            rejectHandler = reject;
            executor(resolve, reject);
        });
        this.#reject = rejectHandler;
        this.isCanceled = false;
        CancelablePromise.#instances.push(this);
    }
    then(onFulfilled, onRejected) {
        const isWrongOnFulfilled = onFulfilled && typeof onFulfilled !== 'function';
        const isWrongOnRejected = onRejected && typeof onRejected !== 'function';
        if(isWrongOnFulfilled || isWrongOnRejected) {
            throw new Error('Wrong callback arguments');
        }
        const promise = super.then(onFulfilled, onRejected);
        promise.cancel = () => this.cancel();
        return promise;
    }
    cancel() {
        this.#reject({ isCanceled: true });
        for (const instance of CancelablePromise.#instances) {
            instance.isCanceled = true;
        }
    }
}

module.exports = CancelablePromise;